package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ResourceBundle;

public class SignIn {

    public WebDriver driver;

    public SignIn(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private static ResourceBundle rb = ResourceBundle.getBundle("credentials");


    @FindBy(id = "ap_email")
    private WebElement fieldEmail;

    @FindBy(id = "ap_password")
    private WebElement fieldPassword;

    @FindBy(id = "signInSubmit")
    private WebElement buttonSigIn;

    public void positiveSigIn(String email, String password){
        fieldEmail.sendKeys(email);
        fieldPassword.sendKeys(password);
        buttonSigIn.click();
    }

    public void test3(){
        positiveSigIn((rb.getString("email")), (rb.getString("password")));
    }
}
