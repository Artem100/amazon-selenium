package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ResourceBundle;

public class Main {
    public WebDriver driver;

    public Main(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private static ResourceBundle rb = ResourceBundle.getBundle("titles"); // To read properties file

    @FindBy(id = "nav-link-accountList")
    private WebElement panelAccountList;

    @FindBy(css = "#nav-flyout-ya-newCust > .nav-a")
    private WebElement buttonStartHere;

    @FindBy(css = "div.nav-signin-tt.nav-flyout")
    private WebElement panelSignIn;

    @FindBy(css = "span.nav-action-inner")
    private WebElement buttonSignIn;

    @FindBy(css = "#nav-link-accountList > .nav-line-1")
    private WebElement accountName;

    @FindBy(css = "#nav-al-your-account > a:nth-child(11)")
    private WebElement buttonPets;

    @FindBy(css = "[data-action='pet-category-selected']:nth-child(2)")
    private WebElement selectPet;

    public void assertMainPage(){
        Assert.assertTrue(driver.getTitle().contains(rb.getString("MainPage")));
    }

    public void navigateOnAccountList(){
        Actions action = new Actions(driver);
        action.moveToElement(panelAccountList).build().perform();
    }

    public void navigateAccountList(){
        assertMainPage();
        navigateOnAccountList();
    }

    public void clickStartHere(){
        navigateAccountList();
        buttonStartHere.click();
    }

    public void clicksignIn(){
        navigateOnAccountList();
        buttonSignIn.click();
    }

    public void checkAccount(){
        assertMainPage();
        Assert.assertTrue(accountName.getText().contains("Hello, Lingla"));
    }

    public void goToPets(){
        navigateOnAccountList();
        buttonPets.click();
    }

}
