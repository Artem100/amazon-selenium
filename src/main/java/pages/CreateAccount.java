package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ResourceBundle;

public class CreateAccount {
    public WebDriver driver;

    public CreateAccount(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private static ResourceBundle rb = ResourceBundle.getBundle("titles");
    private static ResourceBundle rb2 = ResourceBundle.getBundle("errorsCreateAccount");


    @FindBy(id = "ap_customer_name")
    WebElement fieldYourName;

    @FindBy(css = "#ap_customer_name.a-form-error")
    WebElement errorYourNameRequired;

    @FindBy(css = "#auth-customerName-missing-alert > .a-alert-container > .a-alert-content")
    WebElement errorTextRequiredName;

    @FindBy(name = "email")
    WebElement fieldEmail;

    @FindBy(css = "#ap_email.a-form-error")
    WebElement errorEmailRequired;

    @FindBy(css = "#auth-email-missing-alert > .a-alert-container > .a-alert-content")
    WebElement errorTextRequiredEmail;

    @FindBy(name = "password")
    WebElement fieldPassword;

    @FindBy(css = "#ap_password.a-form-error")
    WebElement errorPasswordRequired;

    @FindBy(css = "#auth-password-missing-alert > .a-alert-container > .a-alert-content")
    WebElement errorTextPasswordRequired;

    @FindBy(id = "continue")
    WebElement buttonCreateAccount;

    public void assertPage(){

        Assert.assertTrue(driver.getTitle().contains(rb.getString("CreateAccount")));
    }


    public void errorOfRequiredFields(){
        assertPage();
        buttonCreateAccount.click();
        errorYourNameRequired.isDisplayed();
        Assert.assertTrue((errorTextRequiredName).getText().contains(rb2.getString("requiredYourName")));
        errorEmailRequired.isDisplayed();
        Assert.assertTrue((errorTextRequiredEmail).getText().contains(rb2.getString("requiredEmail")));
        errorPasswordRequired.isDisplayed();
        Assert.assertTrue((errorTextPasswordRequired).getText().contains(rb2.getString("requiredPassword")));

    }





    public void searchField(){
        assertPage();
        fieldYourName.isDisplayed();
    }
}
