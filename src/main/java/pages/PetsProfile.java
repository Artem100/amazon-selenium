package pages;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.Random;
import java.util.ResourceBundle;

public class PetsProfile {

    public WebDriver driver;

    public PetsProfile(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private static ResourceBundle rb = ResourceBundle.getBundle("titles"); // To read properties file


    @FindBy(css = "[data-action='pet-category-selected']:nth-child(1)")
    private WebElement selectDog;

    @FindBy(id = "pet-name-input")
    private WebElement fieldName;

    @FindBy(id = "breed-dropdown-1-pure")
    private WebElement fieldBreed;

    @FindBy(css = "[aria-labelledby='next-button-announce']")
    private WebElement buttonNext1;

    @FindBy(xpath ="//div[@id='step-container-2']//span[@id='next-button']")
    private WebElement buttonNext2;

    @FindBy(id = "finish-button")
    private WebElement buttonFinish;

    @FindBy(id = "yearsAge")
    private WebElement fieldYear;

    @FindBy(id = "monthsAge")
    private WebElement fieldMonth;

    @FindBy(css = ".a-box.a-alert.a-alert-success")
    private WebElement nortificationPet;

    @FindBy(css = "div#coupon_Dog")
    private WebElement dogCoupons;

    @FindBy(css = "[aria-pressed='false']")
    private WebElement breedSelected;

   // @FindBy(xpath = "//span[@data-action='pet-category-selected'][i]")
    //private WebElement selectPet;

    public void assertPetsPage(){
        Assert.assertTrue(driver.getTitle().contains(rb.getString("PetsPage")));
    }

    /*public void clickSelectPet(){
        assertPetsPage();
        Random number = new Random();
        int petsRandom = number.nextInt(30+1);
        String numberPet= String.valueOf(petsRandom);
        String numerOfPet=":nth-child(";
        String endQuery=")";
        String all= numerOfPet+numberPet+endQuery;
        System.out.println(all);
        driver(By(selectPet+all));// find selector for searching pets
    }*/

    public void selectBreed(){
        Random number = new Random();
        int numberId = number.nextInt(12)+1;
        String numberPet = String.valueOf(numberId);
        String selectBreed = "#breed1Pure_"+numberPet;
        WebElement pet = driver.findElement(By.cssSelector(selectBreed));
        pet.click();
    }

    public void selectYear(){
        Random number = new Random();
        int numberId = number.nextInt(15)+1;
        String numberYear = String.valueOf(numberId);
        String selectYear = "#age-years_"+numberYear;
        WebElement year = driver.findElement(By.cssSelector(selectYear));
        year.click();
    }

    public void selectMonth(){
        Random number = new Random();
        int numberId = number.nextInt(12);
        String numberYear = String.valueOf(numberId);
        String selectYear = "#age-months_"+numberYear;
        WebElement month = driver.findElement(By.cssSelector(selectYear));
        month.click();
    }

    public void setSelectDog(){
        assertPetsPage();
        selectDog.click();
        //buttonNext.click();
        fieldName.sendKeys(RandomStringUtils.randomAlphabetic(4));
        buttonNext1.click();
        fieldBreed.click();
        selectBreed();
        breedSelected.isDisplayed();
        buttonNext2.click();
        fieldYear.click();
        selectYear();
        fieldMonth.click();
        selectMonth();
        buttonFinish.click();
        Assert.assertTrue(nortificationPet.isDisplayed());
        Assert.assertTrue(dogCoupons.isDisplayed());
    }

}
