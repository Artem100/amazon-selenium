import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.CreateAccount;
import pages.Main;
import pages.PetsProfile;
import pages.SignIn;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class UI {

    public static WebDriver driver;
    public static Main main;
    public static CreateAccount createAccount;
    public static SignIn signIn;
    public static PetsProfile petsProfile;
    private static ResourceBundle rb = ResourceBundle.getBundle("credentials");

    @BeforeMethod
    public void setup(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://www.amazon.com/");
        main = new Main(driver);
        createAccount = new CreateAccount(driver);
        signIn = new SignIn(driver);
        petsProfile = new PetsProfile(driver);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Test
    public void firstTest(){
        main.clickStartHere();
        createAccount.searchField();
    }

    @Test
    public void requiredFieldsCreateAccount(){
        main.clickStartHere();
        createAccount.errorOfRequiredFields();
    }

    @Test
    public void positiveSignIn(){
        main.clicksignIn();
        signIn.positiveSigIn((rb.getString("email")), (rb.getString("password")));
        main.checkAccount();
    }

    @Test
    public void selectDogAsPet(){
        main.clicksignIn();
        signIn.positiveSigIn((rb.getString("email")), (rb.getString("password")));
        main.goToPets();
        petsProfile.setSelectDog();
    }

    @AfterMethod
    public void close(){
        //driver.quit();
    }
}
